package main

import (
	"archive/tar"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"
)

type bundle struct {
	dir string
}

func newBundle(root string) *bundle {
	dir, err := os.MkdirTemp(root, ".urepo_upload_")
	if err != nil {
		panic(err)
	}

	return &bundle{dir: dir}
}

func (b *bundle) Close() {
	os.RemoveAll(b.dir)
}

func (b *bundle) Receive(r io.Reader) ([]string, error) {
	tr := tar.NewReader(r)

	var extracted []string
	for {
		hdr, err := tr.Next()
		if errors.Is(err, io.EOF) {
			break
		}
		if err != nil {
			return nil, err
		}

		if (hdr.Typeflag != tar.TypeReg) || !strings.HasSuffix(hdr.Name, ".deb") {
			continue
		}

		path := filepath.Join(b.dir, filepath.Base(hdr.Name))
		f, err := os.Create(path)
		if err != nil {
			return nil, err
		}
		_, err = io.Copy(f, tr)
		f.Close()
		if err != nil {
			return nil, err
		}
		extracted = append(extracted, path)
	}

	return extracted, nil
}

func cmdReceive(r io.Reader) error {
	// The SSH_ORIGINAL_COMMAND environment variable contains the
	// original command line when we use restricted commands in
	// authorized_keys. In that case, allow some of the options to
	// be set remotely by running flag.Parse() again. This trick
	// works only if setFlags() uses the current values of the
	// flags as defaults!
	if cmdline := os.Getenv("SSH_ORIGINAL_COMMAND"); cmdline != "" {
		f := flag.NewFlagSet(os.Args[0], flag.ExitOnError)
		setFlags(f)

		// Accept (and ignore) the --receive flag, this makes
		// it possible to use the same command line on the
		// client regardless of whether the server is using
		// forced SSH commands or not.
		f.Bool("receive", false, "")

		args := strings.Fields(cmdline)[1:]
		if err := f.Parse(args); err != nil {
			return err
		}
	}

	b := newBundle(*repoRoot)
	defer b.Close()

	rcvd, err := b.Receive(r)
	if err != nil {
		return fmt.Errorf("receiving bundle: %w", err)
	}

	log.Printf("received %d files in bundle", len(rcvd))

	return cmdImportPackages(rcvd, true)
}
