package main

import (
	"io/fs"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"time"
)

const keep = 3

type fileTS struct {
	path  string
	mtime time.Time
}

type fileTSList []*fileTS

func (l fileTSList) Len() int           { return len(l) }
func (l fileTSList) Swap(i, j int)      { l[i], l[j] = l[j], l[i] }
func (l fileTSList) Less(i, j int) bool { return l[j].mtime.Before(l[i].mtime) }

// Find and remove broken symlinks, corresponding to by-hash entries
// linking to Packages files that have been removed by cleanup().
func cleanupBrokenSymlinks(dir string) error {
	return filepath.WalkDir(dir, func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return nil
		}
		if d.Type()&fs.ModeSymlink != fs.ModeSymlink {
			return nil
		}
		if _, err := os.Stat(path); err != nil {
			return os.Remove(path)
		}
		return nil
	})
}

// Periodically remove old Packages files that might be laying around.
func (r *repo) cleanup() {
	var packages fileTSList

	matches, _ := filepath.Glob(r.root.Join("Packages.*").Abs())
	for _, f := range matches {
		if strings.HasSuffix(f, ".gz") || strings.HasSuffix(f, ".xz") {
			continue
		}

		// Skip symlinks, such as 'Packages' itself.
		stat, _ := os.Lstat(f)
		if !stat.Mode().IsRegular() {
			continue
		}

		packages = append(packages, &fileTS{
			path:  f,
			mtime: stat.ModTime(),
		})
	}

	if len(packages) <= keep {
		return
	}

	sort.Sort(packages)
	for _, pkg := range packages[keep:] {
		matches, _ := filepath.Glob(pkg.path + "*")
		for _, f := range matches {
			log.Printf("removing %s", f)
			os.Remove(f)
		}
	}

	cleanupBrokenSymlinks(r.root.Abs()) //nolint:errcheck
}
